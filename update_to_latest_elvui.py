import json
import os
import shutil
import urllib
import zipfile
import re

ELVUI_HOST = 'www.tukui.org'

def checkForLatestVersion(release="elvui"):
    link = "https://www.tukui.org/dl.php"
    f = urllib.urlopen(link)
    m = f.read()

    search = re.search(release+"-[0-9]{1,2}\.[0-9]{1,2}\.zip", m)
    latestVersion = re.search("[0-9]{1,2}\.[0-9]{1,2}", search.group())
    latestVersion = latestVersion.group()


    print 'Latest Version Detected: ' + str(latestVersion)
    return latestVersion

def cleanUpFiles(removeMe):
    print 'Removing: %s' % (removeMe)
    os.remove(removeMe)


def constructPath(version):
    version = str(version)
    basePath = '/downloads/elvui-'
    return basePath + version + '.zip'

def downloadElvUI(latestVersion, downloadPath):
    print 'Attempting to download.'
    response = urllib.urlopen('http://' + ELVUI_HOST + constructPath(latestVersion))
    with open(str(latestVersion) + '.zip', 'wb') as out_file:
        shutil.copyfileobj(response, out_file)
    print 'Download Completed saved as \'' + str(latestVersion) + '.zip\''

def loadConfig():
    fn = os.path.join(os.path.dirname(__file__), 'config.json')
    rawConfig = open(fn)
    config = json.load(rawConfig)
    rawConfig.close()
    print 'Config Loaded'
    return config

def unzipFile(filePath, extractionDirectory):
    print 'Attempting to unzip'
    zipRef = zipfile.ZipFile(filePath, 'r')
    zipRef.extractall(extractionDirectory)
    zipRef.close()
    print 'Unzipped Succesfully'

def updateConfig(config, latestVersion):
    fn = os.path.join(os.path.dirname(__file__), 'config.json')
    print 'updating config with latest version'
    config['currentVersion'] = str(latestVersion)
    with open(fn, 'w') as outfile:
        json.dump(config, outfile)
    print 'Update Complete'

def check_update_required(currentVersion, latestVersion):
    if currentVersion == latestVersion:
        print 'you are already on the latest version.'
        exit()

config = loadConfig()
latestVersion = checkForLatestVersion()
check_update_required(config['currentVersion'], latestVersion)
downloadElvUI(latestVersion, '')
unzipFile( str(latestVersion) + '.zip', config['wowPath'])
cleanUpFiles(str(latestVersion) + '.zip')
updateConfig(config, latestVersion)
